

// make an express application 
// attach port to that application 

import express, { json } from "express";
import connectToMongoDB from "./src/connectToDb/connectToMongoDb.js";
import bikeRouter from "./src/router/bikeRouter.js";
import bookRouter from "./src/router/bookRouter.js";
import collegeRouter from "./src/router/collegeRouter.js";
import firstRouter from "./src/router/firstRouter.js";
import productRouter from "./src/router/productRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import schoolRouter from "./src/router/schoolsRouter.js";
import secondRouter from "./src/router/secondRouter.js";
import studentRouter from "./src/router/studentRouter.js";
import teacherRouter from "./src/router/teacherRouter.js";
import thirdRouter from "./src/router/thirdRouter.js";
import traineeRouter from "./src/router/traineesRouter.js";
import userRouter from "./src/router/userRouter.js";
import vehicleRouter from "./src/router/vehiclesRouter.js";
import webUserRouter from "./src/router/webUserRouter.js";
import fileRouter from "./src/router/fileRouter.js";



let expressApp = express();
expressApp.use(json());
expressApp.use(express.static("./public"))

expressApp.listen(8000,()=>{

    console.log("express application is listening at port 8000");
});

connectToMongoDB()


expressApp.use("/firsts",firstRouter)

expressApp.use(secondRouter)
expressApp.use(thirdRouter)

expressApp.use("/bikes",bikeRouter)

expressApp.use("/trainees",traineeRouter)
expressApp.use("/schools",schoolRouter)
expressApp.use("/vehicles",vehicleRouter)
expressApp.use("/students",studentRouter)
expressApp.use("/teachers",teacherRouter)

expressApp.use("/books",bookRouter)
expressApp.use("/users",userRouter)
expressApp.use("/colleges",collegeRouter)
expressApp.use("/webUsers",webUserRouter)
expressApp.use("/products",productRouter)
expressApp.use("/reviews",reviewRouter)
expressApp.use("/files",fileRouter)






//always place expressApp.use json at top 
// url =localhost:8000/product?address= nepal-tar 

// url = route?query
//route= localhost:8000/product/a/b
//route= baseUrl/routeParameter1/routeParameter2


//middleware
// function which has (req,res and next) to trigger the next function 


// connect our application via mongoose 

//generate hashPassword 
// let password="ram123"
// let hashPassword=await bcrypt.hash(password,10)
// console.log(hashPassword)


// compare password 
// let hashPassword= "$2b$10$kqDYedxabE1Yegoyazlz7eev67Qoaf0UZbwy4BAfqwWzvxh0LA6MO"
// let password="ram123"
// let isValidPassword = await bcrypt.compare(password,hashPassword)
// console.log(isValidPassword)

// ======>token 

// let  infoObj={
    // name:"prasant",
    // age:22,

//     id: "12345"
// }
// let secretKey ="dw10"
// let  expiryInfo={
//     expiresIn:"356d"
// }

// let token =jwt.sign(infoObj,secretKey,expiryInfo)

// console.log(token)



// let token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoicHJhc2FudCIsImFnZSI6MjIsImlhdCI6MTcwMjAyMTg4OSwiZXhwIjoxNzMyNzgwMjg5fQ.xKZlbSPg4FSGwGydtpeGw99bbgB1OgQzTSWaqDMTyO0"


// try {
//     let infoObj=jwt.verify(token,"dw10")
//     console.log(infoObj)
// } catch (error) {
//     console.log(error.message)
    
// }

//to be verified 
//token must be made form the given secretKey 
//must not expires

//if token is valid => infoObj
//if the token is not valid =>error 


// http://localhost:8000/g1.JPG
// http://localhost:8000/img/g1.JPG




//  let link = [
//       "http://localhost:8000/abc.jpg",
//       "http://localhost:8000/nitan.jpg",
//       "http://localhost:8000/ram.jpg",
//     ]




