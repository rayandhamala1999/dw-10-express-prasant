import { Product } from "../schema/model.js"


export let createProduct= async(req,res)=>{ 
    let productData = req.body
try {
    let result= await Product.create(productData)
    res.json({
        success:true,
        message:" Product  created successfully",
        result: result,
    
    })
    
} catch (error) {
    res.json({
        success:false,
        message:error.message

    })
    
}


}

export let readProduct=async(req,res)=>{
    try {

        let result = await Product.find({})
        res.json({

            success:true,
            message:"Product read successfully ",
            result:result,


        })
        
    } catch (error) {
        res.json({

            success:true,
            message:error.message
           


        })
        
    }
}

export let updateProduct=async(req,res)=>{

    let productId= req.params.productId;
    let productData=req.body;
    try {
        let result= await Product.findByIdAndUpdate(productId,productData)
        res.json({
    
            success:true,
            message:"Product updated successfully",
            result:result
    
    
        })
    
    
    } catch (error) {
    
        res.json({
            success:false,
            message:error.message
        })
        
    }
}

export let deleteProduct=async(req,res)=>{

    let productId =req.params.productId 

    try {
        let result = await Product.findByIdAndDelete(productId)

        res.json({
            success:true,
            message:"Product deleted successfully",
            result:result,
        })


    } catch (error) {

        res.json({
        success: false,
            message: error.message,
           
    })
}


}