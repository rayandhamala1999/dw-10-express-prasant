import collegeRouter from "../router/collegeRouter.js"
import collegeSchema from "../schema/collegeSchema.js"
import { College } from "../schema/model.js"


export let createCollege= async(req,res)=>{ 
    let collegeData = req.body
try {
    let result= await College.create(collegeData)
    res.json({
        success:true,
        message:"college  created successfully",
        result: result,
    
    })
    
} catch (error) {
    res.json({
        success:false,
        message:error.message

    })
    
}


}

export let readCollege=async(req,res)=>{
    try {

        let result = await College.find({})
        res.json({

            success:true,
            message:"college read successfully ",
            result:result,


        })
        
    } catch (error) {
        res.json({

            success:true,
            message:error.message
           


        })
        
    }
}

export let readCollegeDetails=async(req,res)=>

{
    let collegeId=req.params.collegeId

    try {
        let result = await College.findById(collegeId)

        res.json({
            success:true,
            message:"college read successfully",
            result:result,
        })


    } catch (error) {

        res.json({
        success: false,
            message: error.message,
           
    })

    }
}


export let updateCollege=async(req,res)=>{

    let collegeId= req.params.collegeId;
    let collegeData=req.body;
    try {
        let result= await College.findByIdAndUpdate(collegeId,collegeData)
        res.json({
    
            success:true,
            message:"college update successfully",
            result:result
    
    
        })
    
    
    } catch (error) {
    
        res.json({
            success:false,
            message:error.message
        })
        
    }
}

export let deleteCollege=async(req,res)=>{

    let collegeId=req.params.collegeId

    try {
        let result = await College.findByIdAndDelete(collegeId)

        res.json({
            success:true,
            message:"college deleted successfully",
            result:result,
        })


    } catch (error) {

        res.json({
        success: false,
            message: error.message,
           
    })
}


}