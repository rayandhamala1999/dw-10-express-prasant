import { User } from "../schema/model.js"
import bcrypt from "bcrypt"
import { sendEmail } from "../utils/sendemail.js"


export let createUser= async(req,res)=>{ 
    let userData = req.body
    let password = userData.password
try {
let hashPassword =await bcrypt.hash(password,10)
userData.password=hashPassword

    let result= await User.create(userData)


    await sendEmail({
        from :"RD<rayandhamla1999@gmail.com>",
        to:[req.body.email],
        subject:"Email verification.",
        html:"<h1>you have successfully registers</h1>"
    })
    res.status(201).json({
        success:true,
        message:"user created successfully",
        result: result,
    
    })
    
} catch (error) {
    res.json({
        success:false,
        message:error.message

    })
    
}


}

export let readUser=async(req,res)=>{
    try {

        let result = await User.find({})
        res.status(200).json({

            success:true,
            message:"user read successfully ",
            result:result,


        })
        
    } catch (error) {
        res.status(409).json({

            success:true,
            message:error.message
           


        })
        
    }
}

export let readUserDetails=async(req,res)=>

{
    let userId=req.params.userId

    try {
        let result = await User.findById(userId)

        res.json({
            success:true,
            message:"users read successfully",
            result:result,
        })


    } catch (error) {

        res.json({
        success: false,
            message: error.message,
           
    })

    }
}

export let updateUser=async(req,res)=>{

    let userId= req.params.userId;
    let userData=req.body;
    try {
        let result= await User.findByIdAndUpdate(userId,userData)
        res.status(201).json({
    
            success:true,
            message:"users update successfully",
            result:result
    
    
        })
    
    
    } catch (error) {
    
        res.status(400).json({
            success:false,
            message:error.message
        })
        
    }
}

export let deleteUser=async(req,res)=>{

    let userId=req.params.userId

    try {
        let result = await User.findByIdAndDelete(userId)

        res.status(200).json({
            success:true,
            message:"user deleted successfully",
            result:result,
        })


    } catch (error) {

        res.status(404).json({
        success: false,
            message: error.message,
           
    })
}


}

export let loginUser = async (req,res)=>{
let email=req.body.email
let password=req.body.password

try {
    let user = await User.findOne({email:email}) //findOne is used to find the email that is present only on database
    let hashPassword=user.password

    if(user===null){
        res.json({
            success:false,
            message:"Email or password does not match"
        })
    }

    else{
        let isValidPassword=await bcrypt.compare(password,hashPassword)
if(isValidPassword)
{
    res.json({success:true,message:"user login successfully."})
}
else{
    res.json({
        success:false,
        message:"Email or password doesn't  match" //both seems doesn't match because to hackers gets confused from whether there is false  

    })
   }
}

    }
    
catch (error) {

    res.json({
        success:false,
        message:"Email or password  not match"
    })
    

}

}   






// concept

//api
//crud(database)
//hashing
//jwt
//convert file to link 
//save file to server
//send email