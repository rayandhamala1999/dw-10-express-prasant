import { WebUser } from "../schema/model.js"


export let createWebUser= async(req,res)=>{ 
    let webUserData = req.body
try {
    let result= await WebUser.create(webUserData)
    res.json({
        success:true,
        message:" webUser created successfully",
        result: result,
    
    })
    
} catch (error) {
    res.json({
        success:false,
        message:error.message

    })
    
}


}

export let readWebUser=async(req,res)=>{


    let query= req.query 
    let brake=req.brake
    let page =req.page


     try {  

        // let result = await WebUser.find(query)
        let result = await WebUser.find().skip((page-1)*2).limit(brake)


// this is searching 
        // let result = await WebUser.find({age:{$gt:25}})

// this is sorting 

        // this is sorting(unlike js mongodb sorting works for number ) 
        // let result = await WebUser.find({}).sort("name") //ascending
        // let result = await WebUser.find({}).sort("-name") //descending
        // let result = await WebUser.find({}).sort("name age") // here sorting occurs for the name if there there is same thing 
        // then sorting will occur by checking the age 
        // let result = await WebUser.find({}).sort("name -age")
        // let result = await WebUser.find({age:40}).sort("name")



 //selecting 
            //find has control over the object 
            //whereas the select has control over the object property 


             // let result = await WebUser.find({age:40}).select("name")
             // let result = await WebUser.find({age:40}).select("name age")
             // let result = await WebUser.find({age:40}).select("name age -_id")  //id will be disappear 

 //skip
 // let result = await WebUser.find({}).skip("3") 
 
//  limit 
// let result = await WebUser.find({}).limit("5") //only 5 data will appear 
// let result = await WebUser.find({}).limit("5").skip("3")
// let result = await WebUser.find({}).limit("5").skip("2") //skip is always occur at  first 


 
        res.json({

            success:true,
            message:"webUser read successfully ",
            result:result,


        })
        
    } catch (error) {
        res.json({

            success:true,
            message:error.message
           


        })
        
    }
}


export let updateWebUser=async(req,res)=>{

    let webUserId = req.params.webUserId;
    let webUserData = req.body;
    try {
        let result= await WebUser.findByIdAndUpdate(webUserId,webUserData)
        res.json({
    
            success:true,
            message:"WebUser updated successfully",
            result:result
    
    
        })
    
     
    } catch (error) {
    
        res.json({
            success:false,
            message:error.message
        })
        
    }
}


export let deleteWebUser=async(req,res)=>{

    let webUserId=req.params.webUserId

    try {
        let result = await WebUser.findByIdAndDelete(webUserId)

        res.json({
            success:true,
            message:"webUser deleted successfully",
            result:result,
        })


    } catch (error) {

        res.json({
        success: false,
            message: error.message,
           
    })
}


}