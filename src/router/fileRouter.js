import { Router } from "express";
import { createFile } from "../controller/fileController.js";
import upload from "../middleware/upload.js";



let fileRouter =Router()

fileRouter.route("/")
.post(upload.array("document",4),createFile)


export default fileRouter