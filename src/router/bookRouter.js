import { Router } from "express";
import { Book } from "../schema/model.js";


let bookRouter =Router()


bookRouter.
route("/")
.post(async(req,res)=>{ 
    let bookData = req.body
try {
    let result= await Book.create(bookData )
    res.json({
        success:true,
        message:"book  created successfully"
    
    })
    
} catch (error) {
    res.json({
        success:false,
        message:error.message

    })
    
}




})
.get(async(req,res)=>{
    try {

        let result = await Book.find({})
        res.json({

            success:true,
            message:"book read successfully ",
            result:result,


        })
        
    } catch (error) {
        res.json({

            success:true,
            message:error.message
           


        })
        
    }
})

bookRouter.route("/:bookId") //localhost:8000/books/
.delete(async(req,res)=>{

    let bookId=req.params.bookId


    try {
        let result = await Book.findByIdAndDelete(bookId)

        res.json({
            success:true,
            message:" book deleted successfully",
            result:result,
        })


    } catch (error) {

        res.json({
        success: false,
            message: error.message,
           
    })
}


})




export default  bookRouter