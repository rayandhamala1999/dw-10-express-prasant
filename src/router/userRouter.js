import { Router } from "express";
import { User } from "../schema/model.js";
import { createUser, deleteUser, loginUser, readUser, readUserDetails, updateUser } from "../controller/userController.js";


let userRouter=Router()

userRouter.route("/")  
.post(createUser)
.get(readUser)

userRouter.route("/login") //localhost:8000/users/login
.post(loginUser)

userRouter.route("/:userId") //localhost:8000/users/any


.delete(deleteUser)
.get(readUserDetails)
.patch(updateUser)





export default userRouter