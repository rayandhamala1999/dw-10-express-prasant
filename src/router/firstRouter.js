import { Router } from "express";


let firstRouter = Router();

firstRouter
.route("/")  // localhost:8000
.post((req,res,next)=>{

   req.name="prasant",
   req.age=21,
   req.address="tarkeshwor"
    // req.json="ram",


    // console.log(req.name)
    // console.log(req.age)
    // console.log(req.query)
    // console.log("home post")
    next()
})

firstRouter
.route("/name")
.post((req,res)=> {

    res.json("name post")

})

firstRouter
// here we have 
// static route parameter they are product,a
// dynamic route parameter they are name ,b in case of dynamic route parameter we can write any value and written after :symbol 


.route("/product/:name/a/:b")
.post((req,res)=>{

    
    console.log(req.params) 

    //it gives the dynamic route parameter and its values 
    res.json(" i am product ")

})


export default firstRouter;

//making api 
//defining task for each request is called as making api

//send data from post man 