


import { Router } from "express";
// import { createTrainee, deleteTrainee, readTrainee, readTraineeDetails, updateTrainee } from "../controller/traineeController.js";
import { createWebUser, deleteWebUser, readWebUser, updateWebUser } from "../controller/webUserController.js";


let webUserRouter =Router()

webUserRouter
.route("/")  // localhost:8000/trainees




.post(createWebUser)


.get(readWebUser)

webUserRouter.route("/:webUserId") //this is dynamic route and keep it always at last 
.get()
.patch(updateWebUser)
.delete(deleteWebUser)


    





export default webUserRouter