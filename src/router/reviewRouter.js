import { Router } from "express"
import { createReview, deleteReview, readReview, updateReview } from "../controller/reviewController.js"




let reviewRouter =Router()

reviewRouter
.route("/")  // localhost:8000/reviews




.post(createReview)


.get(readReview)

reviewRouter.route("/:reviewId") 
.get()
.patch(updateReview)
.delete(deleteReview)


export default reviewRouter