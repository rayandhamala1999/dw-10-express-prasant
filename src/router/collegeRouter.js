import { Router } from "express"
import { createCollege, deleteCollege, readCollege, readCollegeDetails, updateCollege } from "../controller/collegeController.js"




let collegeRouter=Router()




collegeRouter.route("/")
  
    .post(createCollege)
.get( readCollege)

collegeRouter.route("/:collegeId") //localhost:8000//


.delete(deleteCollege)
.get(readCollegeDetails)
.patch(updateCollege)





export default collegeRouter