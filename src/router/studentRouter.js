import { Router } from "express";
import { Student } from "../schema/model.js";




let studentRouter = Router()

studentRouter.
route("/")
.post(async(req,res)=>{ 
    let studentData = req.body
try {
    let result= await Student.create(studentData)
    res.json({
        success:true,
        message:"student created successfully"
    
    })
    
} catch (error) {
    res.json({
        success:false,
        message:error.message

    })
    
}




})

.get(async(req,res)=>{
    try {

        let result = await Student.find({})
        res.json({

            success:true,
            message:"students read successfully ",
            result:result,


        })
        
    } catch (error) {
        res.json({

            success:true,
            message:error.message
           


        })
        
    }
})

studentRouter.route("/:studentId") //localhost:8000/students/
.delete(async(req,res)=>{

    let studentId=req.params.studentId

    try {
        let result = await Student.findByIdAndDelete(studentId)

        res.json({
            success:false,
            message:"student deleted successfully",
            result:result,
        })


    } catch (error) {

        res.json({
        success: false,
            message: error.message,
           
    })
}


})

.get(async(req,res)=>

{
    let studentId=req.params.studentId

    try {
        let result = await Student.findById(studentId)

        res.json({
            success:true,
            message:"student read successfully",
            result:result,
        })


    } catch (error) {

        res.json({
        success: false,
            message: error.message,
           
    })

    }
})



.patch(async(req,res)=>{

let studentId= req.params.studentId;
let studentData=req.body;
try {
    let result= await Student.findByIdAndUpdate(studentId,studentData)
    res.json({

        success:true,
        message:"students update successfully",
        result:result


    })


} catch (error) {

    res.json({
        success:false,
        message:error.message
    })
    
}

})








export default studentRouter


//data entry and creation syntax 
//Student.create(studentData) 
//Student.find({})
//Student.findById(id)
//Student.findByIdAndDelete(id)
//Student.findByIdAndUpdate(id,data)


//unique:true, is used for the unique email
//update 
//read details
//control f is used to replace all the similar type of name at once 



