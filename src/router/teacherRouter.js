import { Router } from "express"
import { Teacher } from "../schema/model.js"



let teacherRouter = Router()

teacherRouter.
route("/")
.post(async(req,res)=>{ 
    let teacherData = req.body
try {
    let result= await Teacher.create( teacherData )
    res.json({
        success:true,
        message:" teacher created successfully"
    
    })
    
} catch (error) {
    res.json({
        success:false,
        message:error.message

    })
    
}




})
.get(async(req,res)=>{
    try {

        let result = await Teacher.find({})
        res.json({

            success:true,
            message:"Teacher read successfully ",
            result:result,


        })
        
    } catch (error) {
        res.json({

            success:true,
            message:error.message
           


        })
        
    }
})

teacherRouter.route("/:teacherId") //localhost:8000/students/
.delete(async(req,res)=>{

    let teacherId=req.params.teacherId

    try {
        let result = await Teacher.findByIdAndDelete(teacherId)

        res.json({
            success:false,
            message:"teacher deleted successfully",
            result:result,
        })


    } catch (error) {

        res.json({
        success: false,
            message: error.message,
           
    })
}


})


export default  teacherRouter