import { Router } from "express";
import { createTrainee, deleteTrainee, readTrainee, readTraineeDetails, updateTrainee } from "../controller/traineeController.js";


let traineeRouter =Router()

traineeRouter
.route("/")  // localhost:8000/trainees




.post(createTrainee)
.get(readTrainee)

traineeRouter.route("/:traineeId") 
.get(readTraineeDetails)
.patch(updateTrainee)
.delete(deleteTrainee)
    





export default traineeRouter