import { Router } from "express"
import { createProduct, deleteProduct, readProduct, updateProduct } from "../controller/productController.js"



let productRouter =Router()

productRouter
.route("/")  // localhost:8000/trainees




.post(createProduct)


.get(readProduct)

productRouter.route("/:productId") 
.get()
.patch(updateProduct)
.delete(deleteProduct)


    





export default productRouter