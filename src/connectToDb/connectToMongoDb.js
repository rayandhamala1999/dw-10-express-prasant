import mongoose from "mongoose"

let connectToMongoDB = async()=>{

    try {

 await mongoose.connect("mongodb://0.0.0.0:27017/dw10")
 console.log("application is connected to the mongodb database successfully.")
        
    } catch (error) {

        console.log(error.message)
        
    }

    
}

export default connectToMongoDB