


import { Schema } from "mongoose";


let webUserSchema = Schema({

    name:{
        
        type:String,
        required:[true,"name filed is required "]

        //  lowercase:true,
        // uppercase:true,

         //trim:true,//to remove the white spaces present in the sentences or into the name 
       
         // minLength:2,
        // maxLength:20,
        
        // validate: (value) => {
        //     let isValid = /^[a-zA-Z0-9]{2,20}$/.test(value);
      
        //     if (!isValid) {
        //       throw new Error(
        //         "name should not contain other than alphabet and number and must be at least 2 character and at most 20 character long."
        //       );
        //     }
        //   },



    },
    age:{

        type:Number,
        required:[true,"age filed is required "],

        // min:22,
        // max:80,

        // validate:(value)=>{

        //     if(value===30){
        //         throw new Error("not valid for 30")
        //     }

        // }


    },
    password:{
        type:String,
        required:[true,"password field is required "]

    },
    phoneNumber:{
        type:Number,
        required:[true,"phoneNumber field is required "],
        // min:1000000000,
        // max:9999999999,
    
        validate: (value) => {
            let strValue = String(value);
            let strLen = strValue.length;
            if (strLen !== 10) {
              let error = new Error("phoneNUmber  must be exact 10 character long");
              throw error;
            }
          },
        

    },
    roll:{
        type:Number,
        required:[true,"roll field is required "]

    },
    
    isMarried:{
        type:Boolean,
        required:[true,"isMarried field is required "]

    },
    
    spouseName:{
        type:String,
        required:[true,"  spouseName field is required "]

    },
    email:{ 
        type:String,
        // unique:true,
        required:[true,"email filed is required !!"]

    // validate:(value)=>{
    //     let isValid= /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value) //regex for email

    //     if(!isValid){
    //         throw new Error("email must follow the format:example@gamil.com")

    //     }
        

    },


    gender:{
        type:String,
        required:[true,"gender field is required"],
        default:"male",
    //      validate: (value) => {
    //   if (value === "male" || value === "female" || value === "other") {
    //   } else {
    //     throw new Error("gender can not ber other than male, female , other");
    //   }
    // },

    enum: {
        values: ["male", "female", "other"],
        message: (notEnum) => {
          return `${notEnum.value} is not valid enum`;
        },
      },


    },

    dob:{
        type:Date,
        required:[true,"date of birth field is required "]

    },

    location:{

       country:{ type:String,
        required:[true,"location field is required "]
       },
       exactLocation:{
        type:String,
        required:[true,"exactLocation field  is required "]

       }

    },
    favTeacher:[
       {type:String,
        required:[true,"fav teacher field  is required "]

    } 
    ],
    
    favSubject:[
        {
            bookName:{
                type:String,
                required:[true,"bookName field is required "],

            },
            bookAuthor:{
                type:String,
                required:[true,"bookAuthor field  is required "],

            },
        },

],
})

export default webUserSchema