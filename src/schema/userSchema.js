import { Schema } from "mongoose";

let userSchema = Schema({

    name: {
        type:String,
        required:[true,"name field is required !!"]
        
    },
    
    email:{ 
        type:String,
        unique:true,

        required:[true,"email filed is required !!"]
    },
    
    isMarried:{
        type:Boolean,
        required:[true,"isMarried field is required !!"]
    },
    password:{
        type:String,
        required:["password field is required"]
    }
    
    },
    
{
    timestamps: true,
}
    )

    export default userSchema
    