import { Schema } from "mongoose";


let traineeSchema = Schema({

    name:{
        type:String,
        required:[true,"name filed is required "]

    },
    class:{
        type:Number,
        required:[true,"class filed is required "]


    },
    faculty:{
        type:String,
        required:[true,"faculty field is required "]



    }
})

export default traineeSchema