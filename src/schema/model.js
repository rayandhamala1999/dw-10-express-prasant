import { model } from "mongoose";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import traineeSchema from "./traineeSchema.js";
import bookSchema from "./bookSchema.js";
import userSchema from "./userSchema.js";
import collegeSchema from "./collegeSchema.js";
import webUserSchema from "./webUserSchema.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";
import fileSchema from "./fileSchema.js";



export let Student = model ("Student",studentSchema)
export let Teacher = model("Teacher",teacherSchema)
export let Trainee = model("Trainee",traineeSchema)
export let Book = model("Book",bookSchema)
export let User = model("User",userSchema)
export let College = model("College",collegeSchema)
export let WebUser = model("WebUser",webUserSchema)
export let Product = model("Product",productSchema)
export let Review = model("Review",reviewSchema)
export let File = model("File",fileSchema)


//model singular 
//first letter capital 
//match with the variable name  
//model should be in same file and schema in different file 