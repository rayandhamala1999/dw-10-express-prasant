

import { Schema } from "mongoose";


let reviewSchema = Schema({

productId: {
    type:Schema.ObjectId,
    ref:"Product",
    required:[true,"productId field is required !!"]
    
},

userId:{ 
    type:Schema.ObjectId,
    ref:"User",
    required:[true,"userId filed is required !!"]
},

rating:{
    type:Number,
    required:[true,"rating field is required !!"],
    min:1,
    max:10
    
},

description:{
    type:String,
    required:[true,"description field is required !!"]
   

},
},
{
    timestamps: true,
}

)


export default reviewSchema

//Product => name , price, quantity

